const { fontFamily } = require('tailwindcss/defaultTheme');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './app/**/*.{js,ts,jsx,tsx}',
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './src/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      maxWidth: {
        '8xl': '149.5rem',
      },
      boxShadow: {
        primary: '0px 3.2rem 5.4rem rgba(3, 3, 3, 0.135927);',
      },
    },
    screens: {
      sm: {
        max: '37em', // 600px
      },
      md: {
        max: '56.25em', // 900px
      },
      lg: {
        max: '75em', // 1200px
      },
      xl: '112.5em', // 1800px
    },
    typography: {
      '3xl': '2rem',
    },
    fontFamily: {
      sans: ['Inter', ...fontFamily.sans],
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      'gradient-primary': 'var(--tw-gradient-primary)',
      'black-primary': 'var(--tw-black-primary)',
      'black-secondary': 'var(--tw-black-secondary)',
      'white-primary': 'var(--tw-white-primary)',
      'white-secondary': 'var(--tw-white-secondary)',
      'green-primary': 'var(--tw-green-primary)',
      'blue-primary': 'var(--tw-blue-primary)',
      'dark-primary': 'var(--tw-text-dark)',
    },
    keyframes: {
      'fade-in-down': {
        '0%': {
          filter: 'blur(1.2rem);',
          opacity: 0,
        },
        '100%': {
          filter: 'blur(0px);',
          opacity: 1,
        },
      },
      'text-flicker-in-glow': {
        '10%': {
          opacity: 0,
          'text-shadow': ' none',
        },
        '100%': {
          opacity: 1,
          'text-shadow':
            '0 0 30px rgba(255, 255, 255, 0.6), 0 0 60px rgba(255, 255, 255, 0.45), 0 0 110px rgba(255, 255, 255, 0.25), 0 0 100px rgba(255, 255, 255, 0.1)',
        },
      },
    },
    animation: {
      'fade-in-down': 'fade-in-down 1s ease-out',
      'text-flicker-in-glow': 'text-flicker-in-glow 2s linear both',
    },
  },
  plugins: [],
};
