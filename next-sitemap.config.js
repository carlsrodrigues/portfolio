/** @type {import('next-sitemap').IConfig} */
module.exports = {
  siteUrl: 'https://carlos-rodrigues.me',
  generateRobotsTxt: true,
};
