export const data = {
  name: 'Carlos Rodrigues',
  image: 'https://github.com/carls-rodrigues.png',
  description: 'Freelance Developer',
  email: 'cerf@furg.br',
  phone: '+55 51 99999-9999',
  availableForHire: true,
  availableForFreelance: true,
  availability: 'Open for work',
  brand: `I am a software developer based in Brazil, 
    and I am dedicated to creating software with a minimal 
    and aesthetically pleasing design. My goal is to produce 
    software that is both visually appealing and user-friendly,
    with a focus on simplicity and elegance.
  `,
  location: {
    city: 'Rio Grande',
    state: 'RS',
    country: 'Brazil',
  },
  social: {
    github: 'http://github.com/carls-rodrigues',
    linkedin: 'https://www.linkedin.com/in/carlsrodrigues',
    gitlab: 'https://gitlab.com/carlsrodrigues',
  },
};
