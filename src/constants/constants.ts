export const CONSTANTS = {
  socialMedia: [
    {
      name: 'github',
      url: 'https://github.com/carls-rodrigues',
    },
    {
      name: 'gitlab',
      url: 'https://gitlab.com/carlsrodrigues',
    },
    {
      name: 'linkedIn',
      url: 'https://www.linkedin.com/in/carlsrodrigues',
    },
  ],
  worksData: Array.from({ length: 7 }).map((_, index) => ({
    key: index,
    workTitle: [`Work ${index + 1}`, 'Frontend'],
    image: '/images/work.png',
  })),
};
