'use client';

import { createContext, useCallback, useEffect, useState } from 'react';
import { ThemeProvider } from 'styled-components';

import { darkTheme } from '@/styles/dark';
import { lightTheme } from '@/styles/light';
import type { CustomTheme } from '@/types/styled-components';

type ThemeContextProps = {
  isDarkTheme: boolean;
  theme: CustomTheme;
  handleThemeChange: () => void;
};
type ThemeContextProviderProps = {
  children: React.ReactNode;
};

export const ThemeContext = createContext({} as ThemeContextProps);

export function ThemeContextProvider({ children }: ThemeContextProviderProps) {
  const [isDarkTheme, setIsDarkTheme] = useState(() => {
    return window.matchMedia('(prefers-color-scheme: dark)').matches;
  });
  const [theme, setTheme] = useState<CustomTheme>({ ...lightTheme });

  const setDefaultTheme = useCallback(() => {
    setTheme(isDarkTheme ? { ...darkTheme } : { ...lightTheme });
    localStorage.setItem('userTheme', isDarkTheme ? 'dark' : 'light');
  }, [isDarkTheme]);

  const handleThemeChange = () => {
    if (isDarkTheme) {
      setTheme({ ...lightTheme });
      setIsDarkTheme(false);
      localStorage.setItem('userTheme', 'light');
    } else {
      setTheme({ ...darkTheme });
      setIsDarkTheme(true);
      localStorage.setItem('userTheme', 'dark');
    }
  };

  useEffect(() => {
    const userTheme = localStorage.getItem('userTheme');
    if (
      !userTheme ||
      (userTheme && userTheme !== 'light' && userTheme !== 'dark')
    ) {
      setDefaultTheme();
      return;
    }
    if (userTheme === 'dark') {
      setTheme({ ...darkTheme });
      setIsDarkTheme(true);
    } else {
      setTheme({ ...lightTheme });
      setIsDarkTheme(false);
    }
  }, []);
  return (
    <ThemeContext.Provider value={{ theme, handleThemeChange, isDarkTheme }}>
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </ThemeContext.Provider>
  );
}
