import type { InputHTMLAttributes } from 'react';
import { createElement } from 'react';

type InputProps = {
  element?: string;
} & InputHTMLAttributes<HTMLInputElement>;

export default function Input({ element = 'input', ...props }: InputProps) {
  return createElement(element, { ...props }, null);
}
