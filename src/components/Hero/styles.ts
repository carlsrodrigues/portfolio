import styled, { css } from 'styled-components';

import { Box } from '@/components/Box/box';
import Typography from '@/components/Typography';

type ThemeSwitcherProps = {
  isLightTheme: boolean;
};

export const HeroWrapper = styled(Box)``;
export const HeroContent = styled(Box)`
  ${({ theme }) => css`
    flex-direction: column;
    align-items: flex-start;
    padding: 0 5.6rem;
    gap: 3.2rem;
    position: relative;
    animation: fadeIn 3s ease-in-out forwards;
    height: 100vh;
    justify-content: center;
    align-items: center;

    h1 {
      font-size: 5.6rem;
      font-weight: 700;
      color: ${theme.colors.textColor};
      align-self: flex-start;
    }
    button {
      font-size: 2.4rem;
      color: ${theme.colors.primaryColor};
      display: inline-block;
      border: 0.2rem solid ${theme.colors.primaryColor};
      font-weight: 700;
      border-radius: 50%;
      width: 4rem;
      height: 4rem;

      animation: upDown 2s infinite;
      align-self: flex-start;

      svg {
        width: 2.4rem;
        height: 2.4rem;
      }

      @keyframes upDown {
        0% {
          transform: translateY(0);
        }
        50% {
          transform: translateY(-1.5rem);
        }
        100% {
          transform: translateY(0);
        }
      }
    }

    @keyframes fadeIn {
      from {
        opacity: 0;
      }
      to {
        opacity: 1;
      }
    }

    @media screen and (max-width: 37.5rem) {
      padding: 0 1.6rem;
    }
  `}
`;
export const NameTypography = styled(Typography)`
  color: ${({ theme }) => theme.colors.primaryColor};
  text-align: center;
`;
export const ThemeSwitcherButton = styled.button`
  transition: color 300ms;
`;
export const ThemeSwitcher = styled(Box)<ThemeSwitcherProps>`
  position: absolute;
  top: 5rem;
  right: 5rem;
  width: auto;
  gap: 2rem;
  z-index: 1;

  ${ThemeSwitcherButton} {
    ${({ isLightTheme }) => css`
      color: ${isLightTheme ? '#fff' : '#000'};
    `}
  }
`;
