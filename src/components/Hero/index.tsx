import { ArrowDown } from '@styled-icons/fa-solid/ArrowDown';
import { Moon } from '@styled-icons/fa-solid/Moon';
import { Sun } from '@styled-icons/fa-solid/Sun';

import Container from '@/components/Container';
import {
  HeroContent,
  HeroWrapper,
  NameTypography,
  ThemeSwitcher,
  ThemeSwitcherButton,
} from '@/components/Hero/styles';
import Typography from '@/components/Typography';

type HeroProps = {
  isCurrentLightTheme: boolean;
  handleThemeToggle: () => void;
  data: any;
};

export default function HeroSection({
  data,
  isCurrentLightTheme,
  handleThemeToggle,
}: HeroProps) {
  const updateOverflowStatus = () => {
    document.body.style.overflowY = 'auto';
  };

  return (
    <HeroWrapper as='section'>
      <Container>
        <ThemeSwitcher isLightTheme={isCurrentLightTheme}>
          <ThemeSwitcherButton onClick={handleThemeToggle}>
            {isCurrentLightTheme ? <Sun size={24} /> : <Moon size={24} />}
          </ThemeSwitcherButton>
        </ThemeSwitcher>
        <HeroContent>
          <Typography as='h1'>
            Hi, my name is{' '}
            <NameTypography as='span'>{data.name}</NameTypography>
            <br />
            {data.description}
          </Typography>
          <button onClick={updateOverflowStatus}>
            <a href='#about'>
              <ArrowDown />{' '}
            </a>
          </button>
        </HeroContent>
      </Container>
    </HeroWrapper>
  );
}
