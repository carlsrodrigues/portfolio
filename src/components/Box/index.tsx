import type { HTMLAttributes, ReactNode } from 'react';
import { createElement } from 'react';

type BoxElements = 'div' | 'section' | 'main' | 'header' | 'footer';

type BoxProps = {
  children?: ReactNode;
  variant?: BoxElements;
} & HTMLAttributes<HTMLDivElement>;

export default function Box({ variant = 'div', children, ...props }: BoxProps) {
  return createElement(variant, { ...props }, children);
}
