'use client';

import styled, { css } from 'styled-components';

type BoxProps = {
  width?: string | number;
  height?: string | number;
};

export const Box = styled('div')<BoxProps>`
  ${({ width, height }) => css`
    display: flex;
    width: ${width || '100%'};
    height: ${height || 'auto'};
  `}
`;
