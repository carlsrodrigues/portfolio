import type { ReactNode } from 'react';

import { ContentWrapper } from '@/components/Content/styles';
import Typography from '@/components/Typography';

interface ContentProps {
  title: string;
  subtitle: string;
  children: ReactNode;
}

export default function Content({ title, subtitle, children }: ContentProps) {
  return (
    <ContentWrapper>
      <Typography>{title}</Typography>
      <Typography as='h3'>{subtitle}</Typography>
      {children}
    </ContentWrapper>
  );
}
