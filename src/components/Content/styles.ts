'use client';

import styled, { css } from 'styled-components';

import { Box } from '@/components/Box/box';

export const ContentWrapper = styled(Box)`
  ${({ theme }) => css`
    flex-direction: column;
    align-items: center;

    > h3 {
      border-bottom: 0.3rem solid ${theme.colors.primaryColor};
      color: ${theme.colors.textHighLight};
      font-size: 3.6rem;
      margin-bottom: 5rem;
      font-weight: 700;
    }
    > p {
      color: ${theme.colors.textHighLightDark};
      font-size: 1.4rem;
    }
  `}
`;
