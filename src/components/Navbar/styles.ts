'use client';

import Link from 'next/link';
import styled, { css } from 'styled-components';

import { Box } from '@/components/Box/box';

type NavLinkProps = {
  currentActive?: boolean;
  open?: boolean;
};
type NavLinksProps = {
  open?: boolean;
};

const navLinkModifiers = {
  active: (bgColor: string) => css`
    &:before {
      content: '';
      position: absolute;
      bottom: -0.5rem;
      left: 0;
      width: 0;
      height: 0.1rem;
      background-color: ${bgColor};
      transition: width 0.3s ease-in-out;
      width: 100%;
    }
  `,
};
export const Header = styled(Box)`
  ${({ theme }) => css`
    align-items: center;
    justify-content: flex-end;
    padding: 3rem 5rem;
    margin-left: 1rem;
    position: relative;
    background: transparent;
    gap: 2.3rem;
    z-index: 9999;

    ${theme.animate.bounceTop};

    @media (max-width: 37.5em) {
      margin: 0;
    }
  `}
`;
export const NavLink = styled(Link)<NavLinkProps>`
  ${({ theme, currentActive }) => css`
    text-decoration: none;
    color: ${theme.colors.textColor};
    font-weight: 500;
    font-size: 1.5rem;
    display: flex;
    justify-content: center;
    position: relative;
    padding: 0px;
    margin-left: 0.3rem;

    &:hover {
      color: ${theme.colors.textColor};
    }
    &:not(:last-child) {
      margin-right: 2rem;
    }

    &:before {
      content: '';
      position: absolute;
      bottom: -0.5rem;
      left: 0;
      width: 0;
      height: 1px;
      background-color: ${theme.colors.textColor};
      transition: width 0.3s ease-in-out;
    }

    &:hover:before {
      width: 100%;
    }
    ${currentActive && navLinkModifiers.active(theme.colors.textColor)}
  `}
`;
export const NavLinks = styled(Box)<NavLinksProps>`
  width: auto;

  @media (max-width: 37.5em) {
    width: 100%;
    flex-direction: column;
    align-items: center;
    gap: 4rem;
    padding: 3rem 0;

    position: absolute;
    top: 9rem;
    left: 0;
    right: 0;
    background: var(color-primary-background);

    ${NavLink} {
      margin: 0;
    }
    ${({ open }) => css`
      display: ${open ? 'flex' : 'none'};
    `}
  }
`;
export const MobileNav = styled(Box)`
  display: none;
  cursor: pointer;

  @media (max-width: 37.5em) {
    width: auto;
    display: inline-block;
  }
`;
export const ThemeSwitcherWrapper = styled(Box)`
  width: auto;
  gap: 2.3rem;
`;
export const ThemeSwitcher = styled.button`
  color: ${({ theme }) => theme.colors.textColor};
`;
