'use client';

import { BarsStaggered } from '@styled-icons/fa-solid/BarsStaggered';
import { Moon } from '@styled-icons/fa-solid/Moon';
import { Sun } from '@styled-icons/fa-solid/Sun';
import { usePathname } from 'next/navigation';
import { useCallback, useState } from 'react';

import { Box } from '@/components/Box/box';
import {
  Header,
  MobileNav,
  NavLink,
  NavLinks,
  ThemeSwitcher,
  ThemeSwitcherWrapper,
} from '@/components/Navbar/styles';
import useTheme from '@/hooks/useTheme';

export default function Navbar() {
  const { isDarkTheme, handleThemeChange } = useTheme();
  const [isOpen, setIsOpen] = useState(false);
  const navLinks = [
    {
      href: '/',
      label: 'Home',
    },
    {
      href: '/about',
      label: 'About',
    },
    {
      href: '/portfolio',
      label: 'Portfolio',
    },
    {
      href: '/contact',
      label: 'Contact',
    },
  ];
  const pathName = usePathname();

  const toggleMenu = useCallback(() => {
    setIsOpen((prevState) => !prevState);
  }, [isOpen]);

  const toggleTheme = useCallback(() => {
    handleThemeChange();
  }, [isDarkTheme]);
  return (
    <Header as='header'>
      <NavLinks as='nav' open={isOpen}>
        {navLinks.map((link, index) => (
          <NavLink
            key={index}
            href={link.href}
            currentActive={pathName === link.href}
          >
            {link.label}
          </NavLink>
        ))}
      </NavLinks>
      <ThemeSwitcherWrapper>
        <ThemeSwitcher onClick={toggleTheme}>
          {isDarkTheme ? <Moon size={20} /> : <Sun size={20} />}
        </ThemeSwitcher>
      </ThemeSwitcherWrapper>
      <MobileNav>
        <Box onClick={toggleMenu}>
          <BarsStaggered size={24} />
        </Box>
      </MobileNav>
    </Header>
  );
}
