'use client';

import styled, { css } from 'styled-components';

import { Box } from '@/components/Box/box';

export const ContainerWrapper = styled(Box)`
  ${({ theme }) => css`
    background: ${theme.colors.backgroundColor};
    flex-direction: column;
    min-height: 100vh;
    padding: 3rem 5rem;

    > header {
      padding: 0;
    }
  `}
`;
