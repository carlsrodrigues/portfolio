'use client';

import type { ReactNode } from 'react';

import { ContainerWrapper } from '@/components/Container/styles';

interface ContainerProps {
  children: ReactNode;
  as?: 'main' | 'section';
}

export default function Content({ children }: ContainerProps) {
  return <ContainerWrapper>{children}</ContainerWrapper>;
}
