'use client';

import styled from 'styled-components';

type TypographyProps = {
  variant?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'p' | 'span';
};

export const TypographyElement = styled.p<TypographyProps>``;
