import type { HTMLAttributes, ReactNode } from 'react';

import { TypographyElement } from './styles';

type TypographyElements =
  | 'h1'
  | 'h2'
  | 'h3'
  | 'h4'
  | 'h5'
  | 'h6'
  | 'p'
  | 'span';

type TypographyProps = {
  children?: ReactNode;
  as?: TypographyElements;
} & HTMLAttributes<HTMLDivElement>;

export default function Typography({
  as = 'p',
  children,
  ...props
}: TypographyProps) {
  return (
    <TypographyElement as={as} {...props}>
      {children}
    </TypographyElement>
  );
}
