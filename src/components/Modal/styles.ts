import styled from 'styled-components';

import { Box } from '@/components/Box/box';

export const ModalWrapper = styled(Box)`
  height: 100%;
  position: fixed;
  justify-content: center;
  align-items: center;
  inset: 0;
  z-index: 9999;
  background: rgba(0, 0, 0, 0.6);
  backdrop-filter: blur(0.5rem);
`;
