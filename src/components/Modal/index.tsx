'use client';

import { useEffect, useRef } from 'react';
import { createPortal } from 'react-dom';

import { ModalWrapper } from '@/components/Modal/styles';

type ModalProps = {
  children: React.ReactNode;
  isOpen: boolean;
  onClose: () => void;
};

export default function Modal({ children, isOpen, onClose }: ModalProps) {
  const modalRoot = useRef<Element | null>(null);

  const handleCloseModal = () => {
    onClose();
  };

  useEffect(() => {
    if (window !== undefined) {
      modalRoot.current = document.querySelector('#modal-root');
    }
  }, [isOpen]);
  return isOpen && modalRoot.current
    ? createPortal(
        <ModalWrapper onClick={handleCloseModal}>{children}</ModalWrapper>,
        modalRoot.current
      )
    : null;
}
