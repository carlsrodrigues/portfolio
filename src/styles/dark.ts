import animations from './animations';

export const darkTheme = {
  colors: {
    white: '#fff',
    primaryColor: '#02aab0',
    textColor: '#fff',
    textHighLight: '#f3f3f3',
    textHighLightDark: '#f9f9f9',
    backgroundColor: '#101010',
  },
  animate: animations,
};
