import animations from './animations';

export const lightTheme = {
  colors: {
    white: '#fff',
    primaryColor: '#02aab0',
    textColor: '#232741',
    textHighLight: '#232741',
    textHighLightDark: '#232741',
    backgroundColor: '#fff',
  },
  animate: animations,
};
