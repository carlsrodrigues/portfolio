'use client';

import { createGlobalStyle, css } from 'styled-components';

const GlobalStyles = createGlobalStyle`
  ${({ theme }) => css`
    *,
    *::before,
    *::after {
      box-sizing: border-box;
      margin: 0;
      padding: 0;
    }
    html {
      font-size: 62.5%;
      scroll-behavior: smooth;
      background: ${theme.colors.backgroundColor};

      font-family: 'Poppins', sans-serif;
      font-weight: 400;
      font-style: normal;

      @media (max-width: 75em) {
        font-size: 56%;
      }
      @media (max-width: 56.25em) {
        font-size: 50%;
      }
      @media (min-width: 112em) {
        /* font-size: 75%; */
      }
    }
    body {
      width: 100%;
      height: 100%;
      color: ${theme.colors.textColor};
      overflow-x: hidden;
      background: ${theme.colors.backgroundColor};
    }
  `}
`;

export default GlobalStyles;
