'use client';

import '@/styles/globals.scss';

import { Poppins } from 'next/font/google';
import { GoogleAnalytics } from 'nextjs-google-analytics';
import type { ReactNode } from 'react';
import { useEffect, useState } from 'react';

import StyledComponentsRegistry from '@/app/registry';
import Box from '@/components/Box';
import Container from '@/components/Container';
import Navbar from '@/components/Navbar';
import { ThemeContextProvider } from '@/context/ThemeContext';
import GlobalStyles from '@/styles/global';

const poppins = Poppins({ subsets: ['latin'], weight: ['300', '400', '700'] });

export default function RootLayout({ children }: { children: ReactNode }) {
  const [isClient, setIsClient] = useState(false);

  useEffect(() => {
    setIsClient(true);
  }, [isClient]);

  if (!isClient)
    return (
      <html lang='en' className={poppins.className}>
        <body></body>
      </html>
    );
  return (
    <html lang='en' className={poppins.className}>
      <head />
      <body>
        <GoogleAnalytics
          trackPageViews
          gaMeasurementId={process.env.NEXT_PUBLIC_GA_MEASUREMENT_ID}
        />
        <StyledComponentsRegistry>
          <ThemeContextProvider>
            <Container as='main'>
              <GlobalStyles />
              <Navbar />
              {children}
            </Container>
          </ThemeContextProvider>
        </StyledComponentsRegistry>
        <Box id='modal-root' />
      </body>
    </html>
  );
}
