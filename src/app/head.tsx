export default function Head() {
  return (
    <>
      <title>Carlos Rodrigues | Web Developer Portfolio</title>
      <meta
        name='description'
        content="As a skilled software developer and web developer, I specialize in developing high-quality, responsive websites and applications. With expertise in programming languages such as JavaScript, HTML, CSS, and Python, I have built numerous projects that showcase my abilities. Take a look at my portfolio to see examples of my work and contact me to discuss your next project. Let's work together to bring your ideas to life."
        key='description'
      />
      <meta
        name='keywords'
        content='software developer, web developer, responsive websites, applications, JavaScript, HTML, CSS, NextJS, Next, Node, Typescript, experienced'
      />
      <meta name='robots' content='index, follow' />

      <meta
        property='og:title'
        content='Carlos Rodrigues | Web Developer Portfolio'
      />
      <meta property='og:type' content='website' />
      <meta property='og:url' content='https://www.carlos-rodrigues.me/' />
      <meta property='og:image' content='' />
      <meta
        property='og:description'
        content="I'm a skilled web developer with experience in developing custom solutions for a wide range of clients. I specialize in creating robust, scalable applications using the latest technologies, and am passionate about building software that is efficient, secure, and user-friendly. Contact me today to discuss your project and how I can help turn your ideas into reality."
      />
      <meta
        property='og:site_name'
        content='Carlos Rodrigues | Web Developer Portfolio'
      />
      <meta property='og:locale' content='pt_BR' />
      <link rel='canonical' href='https://www.carlos-rodrigues.me/' />
    </>
  );
}
