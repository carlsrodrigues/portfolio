import styled, { css } from 'styled-components';

import { Box } from '@/components/Box/box';

export const AboutWrapper = styled(Box)`
  margin-top: 8rem;
  background: transparent;
  flex-direction: column;
`;

export const AboutContentWrapper = styled(Box)`
  margin: 0 auto;
  max-width: 132rem;
  padding: 0 1rem;
  border-bottom: 0.1rem solid #9f9f9f;
`;

export const AboutContent = styled(Box)`
  max-width: 129.6rem;
  margin: 0 auto;

  & > * {
    box-sizing: border-box;
    flex-shrink: 0;
    width: 100%;
    max-width: 100%;
    padding: 0 1rem;
    margin-top: 0;
  }

  @media (max-width: 56.25em) {
    flex-direction: column;
    align-items: center;
  }
`;

export const AboutPersonalImage = styled(Box)`
  ${({ theme }) => css`
    align-items: center;
    display: flex;
    width: 33rem;
    justify-content: center;
    max-width: 43.2rem;
    margin-bottom: 5rem;
    ${theme.animate.slideInLeft}

    img {
      border-radius: 0.2rem;
      width: 33rem;
      padding: 0.6rem;
      z-index: 50;
      border: 0.1rem solid #9f9f9f;
    }

    @media (max-width: 56.25em) {
      img {
        border-radius: 50%;
        width: 100%;
        width: 25rem;
        height: auto;
        border: none;
      }
    }
  `}
`;

export const AboutPersonalInfo = styled(Box)`
  ${({ theme }) => css`
    flex: 1;
    display: flex;
    flex-direction: column;
    ${theme.animate.slideInRight}

    h4 {
      color: ${theme.colors.textHighLight};
      font-size: 1.8rem;
      font-weight: 400;
      margin-bottom: 1.5rem;
    }

    h5 {
      color: ${theme.colors.textColor};
      font-size: 2rem;
      margin-bottom: 1.5rem;
      font-weight: 500;
    }
    p {
      color: #9f9f9f;
      font-size: 1.5rem;
    }
  `}
`;

export const AboutPersonalContact = styled(Box)`
  ${({ theme }) => css`
    border-top: 0.1rem solid #9f9f9f;
    margin-top: 3rem;
    padding-top: 3rem;
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    row-gap: 1rem;

    span {
      font-size: 1.5rem;
      color: ${theme.colors.textHighLight};
    }
    a {
      cursor: pointer;
      p {
        text-decoration: underline;
        color: ${theme.colors.textColor};
      }
    }
    p {
      display: inline-block;
      margin-left: 1rem;
      color: #9f9f9f;
      font-size: 1.5rem;
    }
    @media (max-width: 37.5em) {
      grid-template-columns: repeat(1, 1fr);
      grid-template-rows: auto;
    }
  `}
`;

export const AboutPersonalSocial = styled(Box)`
  ${({ theme }) => css`
    height: 10rem;
    justify-content: space-between;
    align-items: center;

    button {
      display: inline-block;
      background-color: ${theme.colors.primaryColor};
      border-radius: 2rem;
      border: none;
      color: ${theme.colors.white};
      cursor: pointer;
      font-size: 1.4rem;
      margin-top: 1rem;
      padding: 1.2rem 3rem;
    }

    > div {
      gap: 2rem;
      width: auto;
    }

    @media (max-width: 37.5em) {
      flex-direction: column;
      align-items: flex-start;
      margin-top: 2rem;

      > div {
        display: none;
      }
    }
  `}
`;
