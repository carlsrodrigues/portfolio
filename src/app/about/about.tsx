'use client';

import { GithubSquare } from '@styled-icons/fa-brands/GithubSquare';
import { Gitlab } from '@styled-icons/fa-brands/Gitlab';
import { Linkedin } from '@styled-icons/fa-brands/Linkedin';
import Image from 'next/image';
import Link from 'next/link';

import {
  AboutContent,
  AboutContentWrapper,
  AboutPersonalContact,
  AboutPersonalImage,
  AboutPersonalInfo,
  AboutPersonalSocial,
  AboutWrapper,
} from '@/app/about/styles';
import { Box } from '@/components/Box/box';
import Content from '@/components/Content';
import Typography from '@/components/Typography';

type AboutProps = {
  data: any;
};

export default function About({ data }: AboutProps) {
  const curriculumOutputName = `${data.name}-Resume-${Date.now()}.pdf`;
  const handleFetchCurriculumData = async () => {
    const fileUrl = '/files/resume.pdf';
    const link = document.createElement('a');
    link.href = fileUrl;
    link.setAttribute('download', curriculumOutputName);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <AboutWrapper as='section'>
      <Content title='Let me introduce myself' subtitle='About Me'>
        <AboutContentWrapper>
          <AboutContent>
            <AboutPersonalImage>
              <Image src={data.image} alt='About' width={330} height={330} />
            </AboutPersonalImage>
            <AboutPersonalInfo>
              <Typography as='h4'>Nice to meet you</Typography>
              <Typography as='h5'>
                Frontend Web Developer who creates amazing digital experiences!
              </Typography>
              <Typography>{data.brand}</Typography>
              <AboutPersonalContact>
                <Box>
                  <Typography as='span'>Name:</Typography>
                  <Typography>{data.name}</Typography>
                </Box>
                <Box>
                  <Typography as='span'>Email:</Typography>
                  <Link href={`mailto:${data.email}`}>
                    <Typography>{data.email}</Typography>
                  </Link>
                </Box>
                <Box>
                  <Typography as='span'>Location:</Typography>
                  <Typography>{`${data.location.city}, ${data.location.country}`}</Typography>
                </Box>
                <Box>
                  <Typography as='span'>Availability:</Typography>
                  <Typography>{data.availability}</Typography>
                </Box>
              </AboutPersonalContact>
              <AboutPersonalSocial>
                <button onClick={handleFetchCurriculumData}>
                  Download Resume
                </button>
                <Box>
                  <Link href={data.social.github} target='_blank'>
                    <GithubSquare size={24} />
                  </Link>
                  <Link href={data.social.gitlab} target='_blank'>
                    <Gitlab size={24} />
                  </Link>
                  <Link href={data.social.linkedin} target='_blank'>
                    <Linkedin size={24} />
                  </Link>
                </Box>
              </AboutPersonalSocial>
            </AboutPersonalInfo>
          </AboutContent>
        </AboutContentWrapper>
      </Content>
    </AboutWrapper>
  );
}
