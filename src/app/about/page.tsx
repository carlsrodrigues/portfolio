import About from '@/app/about/about';
import { data } from '@/config/config';

export const metadata = {
  title: 'About Me',
};

export default function Page() {
  return <About data={data} />;
}
