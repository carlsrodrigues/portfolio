import styled, { css } from 'styled-components';

import { Box } from '@/components/Box/box';

export const Content = styled(Box)`
  align-items: center;
  background: transparent;
  flex-direction: column;
  flex: 1;
  justify-content: center;
  position: relative;
`;
export const Title = styled.h1`
  ${({ theme }) => css`
    color: rgb(255, 255, 255);
    font-size: 7rem;
    font-weight: 700;
    margin-top: -10rem;
    opacity: 1;
    padding-bottom: 2.8rem;
    transform: translateY(0vw) translateZ(0px);
    color: ${theme.colors.textColor};
    ${theme.animate.slideInTop}

    @media (max-width: 37.5em) {
      text-align: center;
    }
  `}
`;
export const SubTitle = styled.h3`
  ${({ theme }) => css`
    font-size: 3.2rem;
    opacity: 1;
    transform: translateY(0vw) translateZ(0px);
    color: ${theme.colors.textColor};
    ${theme.animate.slideInLeft}

    @media (max-width: 37.5em) {
      text-align: center;
    }
  `}
`;
export const SocialIcons = styled(Box)`
  ${({ theme }) => css`
    bottom: 0;
    flex-direction: column;
    gap: 2rem;
    left: 0;
    position: absolute;
    width: auto;

    svg {
      color: ${theme.colors.textColor};
    }

    ${theme.animate.slideInLeft}

    a {
      @keyframes icons-animated {
        0% {
          transform: translateY(0);
        }
        50% {
          transform: translateY(-0.5rem);
        }
        100% {
          transform: translateY(0);
        }
      }

      &:first-child {
        animation: icons-animated infinite 1.5s ease-in-out;
      }

      &:nth-child(2) {
        animation: icons-animated infinite 1.75s ease-in-out;
      }
      &:last-child {
        animation: icons-animated infinite 2s ease-in-out;
      }
    }

    @media (max-width: 37.5em) {
      flex-direction: row;
      left: 33%;
      gap: 3rem;
    }
  `}
`;
