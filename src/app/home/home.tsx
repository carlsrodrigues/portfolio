'use client';

import { GithubSquare } from '@styled-icons/fa-brands/GithubSquare';
import { Gitlab } from '@styled-icons/fa-brands/Gitlab';
import { Linkedin } from '@styled-icons/fa-brands/Linkedin';
import Link from 'next/link';

import { Content, SocialIcons, SubTitle, Title } from '@/app/home/styles';

type HomeProps = {
  data: any;
};

export default function Home({ data }: HomeProps) {
  return (
    <Content as='section'>
      <Title>{data.name}</Title>
      <SubTitle>{data.description}</SubTitle>
      <SocialIcons>
        <Link href={data.social.github} target='_blank'>
          <GithubSquare size={24} />
        </Link>
        <Link href={data.social.gitlab} target='_blank'>
          <Gitlab size={24} />
        </Link>
        <Link href={data.social.linkedin} target='_blank'>
          <Linkedin size={24} />
        </Link>
      </SocialIcons>
    </Content>
  );
}
