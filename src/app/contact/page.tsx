import Contact from '@/app/contact/contact';
import { data } from '@/config/config';

export const metadata = {
  title: 'Contact Me',
};

export default function Page() {
  return <Contact data={data} />;
}
