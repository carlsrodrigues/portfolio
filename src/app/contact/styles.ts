import styled from 'styled-components';

import { Box } from '@/components/Box/box';

type ContactFormInputProps = {
  area?: string;
};

export const ContactWrapper = styled(Box)`
  margin-top: 8rem;
  background: transparent;
  flex-direction: column;
`;

export const ContactContentWrapper = styled(Box)`
  margin: 0 auto;
  max-width: 132rem;
  padding: 0 1rem;

  @media (max-width: 56.25em) {
    flex-direction: column;
    max-width: 72rem;
    gap: 3rem;
    padding: 0;
  }
`;

export const ContactFormWrapper = styled(Box)`
  flex: 1;
  flex-direction: column;
  ${({ theme }) => theme.animate.slideInLeft}
  padding: 0 2.5rem;

  h4 {
    color: ${({ theme }) => theme.colors.textHighLight};
    padding-bottom: 1rem;
    font-size: 1.6rem;
    font-weight: 700;
  }
  @media (max-width: 56.25em) {
    padding: 0;
  }
`;
export const ContactForm = styled.form`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(4, auto);
  grid-template-areas: 'name email' 'subject subject' 'message message' 'button button';
  grid-gap: 2rem 1.5rem;

  button {
    background: ${({ theme }) => theme.colors.primaryColor};
    border: none;
    border-radius: 2rem;
    color: ${({ theme }) => theme.colors.textHighLightDark};
    cursor: pointer;
    font-size: 1.4rem;
    padding: 1.2rem 3rem;
    max-width: 17rem;
  }

  @media (max-width: 56.25em) {
    grid-template-columns: 1fr;
    grid-template-rows: repeat(5, auto);
    grid-template-areas:
      'name'
      'email'
      'subject'
      'message'
      'button';
  }
`;
export const ContactFormInput = styled.input<ContactFormInputProps>`
  background-color: #161616;
  border: none;
  border-bottom: 0.1rem solid #9f9f9f;
  border-radius: 0.1rem;
  color: #9f9f9f;
  font-size: 1.5rem;
  grid-area: ${({ area }) => area};
  outline: transparent;
  padding: 1rem;
  width: 100%;

  &::placeholder {
    color: #9f9f9f;
    font-size: 1.5rem;
  }
`;
export const ContactFormTextArea = styled.textarea<ContactFormInputProps>`
  background-color: #161616;
  border: none;
  border-bottom: 0.1rem solid #9f9f9f;
  border-radius: 0.1rem;
  color: #9f9f9f;
  font-size: 1.5rem;
  grid-area: ${({ area }) => area};
  height: auto;
  min-height: 10rem;
  outline: transparent;
  padding: 1rem;
  resize: none;
  width: 100%;
`;
export const ContactFormButton = styled.button``;
export const ContactInfoWrapper = styled(Box)`
  flex: 1;
  padding: 0 1rem;
`;
export const ContactInfo = styled(Box)`
  flex-direction: column;
  ${({ theme }) => theme.animate.slideInRight}

  h5 {
    font-size: 1.5rem;
    color: ${({ theme }) => theme.colors.textHighLight};
    padding-bottom: 1rem;
    font-weight: 700;
  }
  p {
    margin-bottom: 3rem;
    font-size: 1.5rem;
  }
`;
export const ContactInfoPersonal = styled(Box)`
  flex-direction: column;
  gap: 3.4rem;
`;
export const ContactInfoPersonalItem = styled(Box)`
  div {
    width: auto;
    flex-direction: column;

    &:first-child {
      width: 5rem;
      justify-content: center;
    }
  }
  h6 {
    color: ${({ theme }) => theme.colors.textHighLight};
    font-size: 1.5rem;
  }
  span {
    font-size: 1.5rem;
    color: #9f9f9f;
  }
`;
export const ContactInfoSocialWrapper = styled(Box)`
  justify-content: center;
  align-items: center;
  gap: 2rem;
  margin-top: 5rem;

  &::before {
    content: '';
    width: 5rem;
    height: 0.2rem;
    display: inline-block;
    background: #9f9f9f;
  }
  &::after {
    content: '';
    width: 5rem;
    height: 0.2rem;
    display: inline-block;
    background: #9f9f9f;
  }

  @media (max-width: 37.5em) {
    display: none;
  }
`;
