'use client';

import { GithubSquare } from '@styled-icons/fa-brands/GithubSquare';
import { Gitlab } from '@styled-icons/fa-brands/Gitlab';
import { Linkedin } from '@styled-icons/fa-brands/Linkedin';
import { Envelope } from '@styled-icons/fa-solid/Envelope';
import { LocationPin } from '@styled-icons/fa-solid/LocationPin';
import { User } from '@styled-icons/fa-solid/User';
import Link from 'next/link';

import {
  ContactContentWrapper,
  ContactForm,
  ContactFormInput,
  ContactFormTextArea,
  ContactFormWrapper,
  ContactInfo,
  ContactInfoPersonal,
  ContactInfoPersonalItem,
  ContactInfoSocialWrapper,
  ContactInfoWrapper,
  ContactWrapper,
} from '@/app/contact/styles';
import { Box } from '@/components/Box/box';
import Content from '@/components/Content';
import Typography from '@/components/Typography';

type ContactProps = {
  data: any;
};

export default function Contact({ data }: ContactProps) {
  return (
    <ContactWrapper>
      <Content title='Get in touch' subtitle='Contact'>
        <ContactContentWrapper>
          <ContactFormWrapper>
            <Typography as='h4'>Message me</Typography>
            <ContactForm>
              <ContactFormInput area='name' placeholder='Name' type='text' />
              <ContactFormInput area='email' placeholder='Email' type='email' />
              <ContactFormInput
                area='subject'
                placeholder='Subject'
                type='text'
              />
              <ContactFormTextArea
                area='message'
                placeholder='Message'
                maxLength={500}
              />
              <button>Send Message</button>
            </ContactForm>
          </ContactFormWrapper>
          <ContactInfoWrapper>
            <ContactInfo>
              <Typography as='h5'>Contact Information</Typography>
              <Typography>
                Open for opportunities. Let&apos;s connect and build something
                awesome together!
              </Typography>
              <ContactInfoPersonal>
                <ContactInfoPersonalItem>
                  <Box>
                    <User size='28' />
                  </Box>
                  <Box>
                    <Typography as='h6'>Name</Typography>
                    <Typography as='span'>{data.name}</Typography>
                  </Box>
                </ContactInfoPersonalItem>
                <ContactInfoPersonalItem>
                  <Box>
                    <LocationPin size='28' />
                  </Box>
                  <Box>
                    <Typography as='h6'>Location</Typography>
                    <Typography as='span'>{`${data.location.city}, ${data.location.country}`}</Typography>
                  </Box>
                </ContactInfoPersonalItem>
                <ContactInfoPersonalItem>
                  <Box>
                    <Envelope size='28' />
                  </Box>
                  <Box>
                    <Typography as='h6'>Email</Typography>
                    <Link href={`mailto:${data.email}`}>
                      <Typography as='span'>{data.email}</Typography>
                    </Link>
                  </Box>
                </ContactInfoPersonalItem>
              </ContactInfoPersonal>
            </ContactInfo>
          </ContactInfoWrapper>
        </ContactContentWrapper>
        <ContactInfoSocialWrapper>
          <Link href={data.social.github} target='_blank'>
            <GithubSquare size={24} />
          </Link>
          <Link href={data.social.gitlab} target='_blank'>
            <Gitlab size={24} />
          </Link>
          <Link href={data.social.linkedin} target='_blank'>
            <Linkedin size={24} />
          </Link>
        </ContactInfoSocialWrapper>
      </Content>
    </ContactWrapper>
  );
}
