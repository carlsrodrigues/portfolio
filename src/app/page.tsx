'use client';

import { useCallback, useState } from 'react';
import Particles from 'react-particles';
import type { Engine } from 'tsparticles-engine';
import { loadLinksPreset } from 'tsparticles-preset-links';

import Home from '@/app/home/home';
import { data } from '@/config/config';
import tsParticlesConfig from '@/config/tsparticles-config';
import useTheme from '@/hooks/useTheme';

export default function Page() {
  const { theme } = useTheme();
  const [particlesInitiated, setParticlesInitiated] = useState(false);
  const customInit = useCallback(async (engine: Engine) => {
    await loadLinksPreset(engine);
    setParticlesInitiated(true);
  }, []);
  const handleParticlesColor = useCallback(() => {
    return {
      particleColor: theme.colors.textColor,
      lineLinked: theme.colors.textColor,
    };
  }, [theme]);
  return (
    <>
      <Particles
        id='tsparticles'
        options={tsParticlesConfig(handleParticlesColor())}
        init={customInit}
        className='absolute inset-0'
      />
      {particlesInitiated && <Home data={data} />}
    </>
  );
}
