import Portfolio from '@/app/portfolio/portfolio';

export const metadata = {
  title: 'Portfolio',
};

export default function Page() {
  return <Portfolio />;
}
