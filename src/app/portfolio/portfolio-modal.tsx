import Link from 'next/link';

import { PortfolioModalWrapper } from '@/app/portfolio/styles';
import Typography from '@/components/Typography';

export type PortfolioModalProps = {
  data: any;
};
export default function PortfolioModal({ data }: PortfolioModalProps) {
  return (
    <PortfolioModalWrapper>
      <Typography as='h3'>{data.title}</Typography>
      <Typography>{data.description}</Typography>
      <Link href={data.links.repository} target='_blank'>
        Github
      </Link>
      <Link href={data.links.live} target='_blank'>
        Live Link
      </Link>
    </PortfolioModalWrapper>
  );
}
