'use client';

import { useState } from 'react';

import PortfolioModal from '@/app/portfolio/portfolio-modal';
import {
  PortfolioContainer,
  PortfolioItem,
  PortfolioLeft,
  PortfolioRight,
  PortfolioTechnologies,
  PortfolioTitle,
  PortfolioWrapper,
} from '@/app/portfolio/styles';
import Content from '@/components/Content';
import Modal from '@/components/Modal';
import Typography from '@/components/Typography';

export default function Portfolio() {
  const [openModal, setOpenModal] = useState(false);
  const [selectedWork, setSelectedWork] = useState<any>({});
  const works = [
    {
      id: 1,
      title: 'Codeflix',
      technologies: 'React | Next.js | TypeScript',
      image: '/images/bike.png',
      description:
        "This dashboard uses the OpenWeatherMap API to retrieve detailed weather data, including the city name, date, weather icon, temperature, humidity, and wind speed. The dashboard has a 5-day forecast feature and a search history feature that saves past searches for easy access. It also has a dark/light theme that changes the look of the page based on day or night in the searched location and uses the geolocation API to automatically determine the user's current location and display the weather.",
      links: {
        repository: 'https://github.com/carls-rodrigues',
        live: 'https://www.carlos-rodrigues.me/',
      },
    },
    {
      id: 2,
      title: 'Flux Money Expense Tracker',
      technologies: 'React | Next.js | TypeScript',
      image: '/images/bike.png',
      description:
        "This dashboard uses the OpenWeatherMap API to retrieve detailed weather data, including the city name, date, weather icon, temperature, humidity, and wind speed. The dashboard has a 5-day forecast feature and a search history feature that saves past searches for easy access. It also has a dark/light theme that changes the look of the page based on day or night in the searched location and uses the geolocation API to automatically determine the user's current location and display the weather.",
      links: {
        repository: 'https://github.com/carls-rodrigues',
        live: 'https://www.carlos-rodrigues.me/',
      },
    },
    {
      id: 3,
      title: 'Shorten',
      technologies: 'React | Next.js | TypeScript',
      image: '/images/bike.png',
      description:
        "This dashboard uses the OpenWeatherMap API to retrieve detailed weather data, including the city name, date, weather icon, temperature, humidity, and wind speed. The dashboard has a 5-day forecast feature and a search history feature that saves past searches for easy access. It also has a dark/light theme that changes the look of the page based on day or night in the searched location and uses the geolocation API to automatically determine the user's current location and display the weather.",
      links: {
        repository: 'https://github.com/carls-rodrigues',
        live: 'https://www.carlos-rodrigues.me/',
      },
    },
  ];

  const handleOpenModal = (work: any) => {
    setOpenModal((prevState) => !prevState);
    setSelectedWork(work);
  };
  return (
    <PortfolioWrapper>
      <Modal isOpen={openModal} onClose={() => handleOpenModal({})}>
        <PortfolioModal data={selectedWork} />
      </Modal>
      <Content title='View my work' subtitle='Portfolio'>
        <PortfolioContainer>
          {works.map((work, index) => (
            <PortfolioItem
              key={index}
              index={index}
              onClick={() => handleOpenModal(work)}
            >
              <PortfolioLeft>
                <PortfolioTechnologies>
                  {work.technologies}
                </PortfolioTechnologies>
                <PortfolioTitle>{work.title}</PortfolioTitle>
                <Typography as='span'>View Work →</Typography>
              </PortfolioLeft>
              <PortfolioRight>
                <img src={work.image} alt='' />
              </PortfolioRight>
            </PortfolioItem>
          ))}
        </PortfolioContainer>
      </Content>
    </PortfolioWrapper>
  );
}
