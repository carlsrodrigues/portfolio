import styled, { css } from 'styled-components';

import { Box } from '@/components/Box/box';

type PortfolioItemProps = {
  index: number;
};

export const PortfolioWrapper = styled(Box)`
  margin-top: 8rem;
  background: transparent;
  flex-direction: column;
`;
export const PortfolioContainer = styled(Box)`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 2.4rem;

  @media (max-width: 75em) {
    grid-template-columns: 1fr;
  }
`;
export const PortfolioLeft = styled(Box)`
  ${({ theme }) => css`
    align-items: center;
    justify-content: center;
    flex-direction: column;
    transition: all 0.9s ease-in-out;
    z-index: 1;
    margin: 4rem;

    span {
      font-size: 1.6rem;
      color: ${theme.colors.textColor};
      cursor: pointer;
    }
  `}
`;
export const PortfolioRight = styled(Box)`
  transform: translateX(30%);
  transition: all 0.3s ease-in-out;
  align-items: center;
  justify-content: flex-end;

  img {
    max-width: 32rem;
    max-height: 19.2rem;
    margin-top: 2.5rem;
    transition: all 1.3s ease-in-out;
  }
`;
export const PortfolioItem = styled(Box)<PortfolioItemProps>`
  ${({ theme, index }) => css`
    background: rgb(41, 203, 224);
    border-radius: 0.3rem;
    color: ${theme.colors.textColor};
    cursor: pointer;
    flex: 1;
    height: 35rem;
    padding: 0;
    position: relative;
    transition: all 0.5s ease-in-out;
    padding: 3rem;
    overflow: hidden;
    ${index % 2 === 0 ? theme.animate.slideInLeft : theme.animate.slideInRight};

    &::before {
      background-color: rgba(0, 0, 0, 0.7);
      bottom: 0;
      content: '';
      display: block;
      left: 0;
      opacity: 0.3;
      position: absolute;
      right: 0;
      top: 0;
      transition: opacity 0.3s ease-in-out;
    }

    &:hover {
      ${PortfolioRight} {
        transform: translateX(-70%);
        img {
          scale: 1.4;
        }
      }
      ${PortfolioLeft} {
        transform: translateX(-250%);
      }
    }

    @media (max-width: 37.5em) {
      flex-direction: column;
      height: 42rem;
      justify-content: space-between;
      animation: none;

      ${PortfolioLeft} {
        margin: 0;
      }
      ${PortfolioRight} {
        justify-content: center;
        transform: translateX(0);
        img {
          margin: 0;
          justify-content: center;
        }
      }

      &:hover {
        ${PortfolioRight} {
          transform: none;
          img {
            scale: 1.3;
          }
        }
        ${PortfolioLeft} {
          transform: none;
        }
      }
    }
  `}
`;
export const PortfolioTitle = styled.h3`
  font-size: 3rem;
  padding: 1rem 0;
  font-weight: 700;
  text-shadow: none;
`;
export const PortfolioTechnologies = styled.p`
  font-size: 1.4rem;
  white-space: nowrap;
  font-style: italic;
`;
export const PortfolioModalWrapper = styled(Box)`
  ${({ theme }) => css`
    width: 40rem;
    background: ${theme.colors.backgroundColor};
    padding: 6rem;
    flex-direction: column;

    > h3 {
      color: ${theme.colors.textColor};
      text-align: center;
      font-size: 1.9rem;
      font-weight: 700;
      margin-bottom: 1rem;
    }
    > p {
      font-size: 1.6rem;
      margin-bottom: 1rem;
      color: rgb(159, 159, 159);
    }

    a {
      display: inline-block;
      background-color: ${theme.colors.primaryColor};
      border-radius: 2rem;
      border: none;
      color: ${theme.colors.textHighLightDark}};
      cursor: pointer;
      font-size: 1.4rem;
      margin-top: 1rem;
      padding: 1.2rem 3rem;
      text-align: center;
    }
  `}
`;
